<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
</head>

<body>
    <?php


    $arrayArticulos = array(
        array("Codigo" => 1, "Descripcion" => "Arroz", "Existencias" => 9),
        array("Codigo" => 5, "Descripcion" => "Detergente", "Existencias" => 32),
        array("Codigo" => 15, "Descripcion" => "Ketchup", "Existencias" => 15),
        array("Codigo" => 10, "Descripcion" => "Carne", "Existencias" => 20)
    );

    function mayorExistencias($arrayArticulos)
    {
        $maxExistencia = 0;
        foreach ($arrayArticulos as $articulo) {
            if ($articulo["Existencias"] > $maxExistencia) {
                $descripcion = $articulo["Descripcion"];
                $maxExistencia = $articulo["Existencias"];
            }
        }
        return $descripcion;
    }

    function sumarExistencias($arrayArticulos)
    {
        foreach ($arrayArticulos as $articulo) {
            $existencias = $articulo["Existencias"];
        }
        return $existencias;
    }

    function mostrarArray($arrayArticulos)
    {
        for ($i = 0; $i < count($arrayArticulos); $i++) {
            print $arrayArticulos[$i]["Codigo"] . " " . $arrayArticulos[$i]["Descripcion"] . " " .
                $arrayArticulos[$i]["Existencias"] . "<br>";
        }
    }

    print mayorExistencias($arrayArticulos) . "<br>";
    print sumarExistencias($arrayArticulos) . "<br>";
    mostrarArray($arrayArticulos);

    ?>
</body>

</html>