<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->

<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
       
        <form method="POST" action="<?php echo htmlspecialchars($_SERVER['PHP_SELF']) ?>">
            
            <fieldset> <legend>Conversor de monedas: </legend>
                <p><label>Introduzca una cantidad: </label> <input type="number" name="cantidad" value="<?php echo $_POST['cantidad'] ?>" /> </p>
                <p>  
                   <label for="browser">Origen: </label>
                <input list="uno" name="origen" id="browser">

                <datalist id="uno">
                  <option value="Euro">
                  <option value="Dollar">
                  <option value="Libra">
                 
                </datalist>
                </p>  

                <p>  <label for="browser">Destino: </label>
                <input list="dos" name="destino" id="browser">

                <datalist id="dos">
                  <option value="Euro">
                  <option value="Dollar">
                  <option value="Libra">
                  
                </datalist>
                
                </p>   
           </fieldset>
            <input type="submit" value="Convertir" name="convertir" />
        </form>
        
         <?php
        
        if (isset($_POST["convertir"])) {
        $cantidad=$_POST["cantidad"];
        $origen=$_POST["origen"];
        $destino=$_POST["destino"];
        
        $valorDollar=array("Euro"=>0.84, "Dollar"=>1, "Libra"=>0.772);
        $valorEuro=array("Euro"=>1, "Dollar"=>1.17, "Libra"=>0.91);
        $valorLibra=array("Euro"=>1.09, "Dollar"=>1.30, "Libra"=>1);
        
        $bidimensionalmonedas=array("Euro"=>$valorEuro, "Libra"=>$valorLibra, "Dollar"=>$valorDollar);
   
        $calculo=$cantidad*$bidimensionalmonedas[$origen][$destino];
        
        echo "La cantidad $cantidad de $origen son $calculo $destino ";
        }
        
        
        ?>
    </body>
</html>
