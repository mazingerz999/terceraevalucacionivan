<?php 
require_once( 'funciones.php' ); 
session_start();


 echo "<h3>Bienvenido  {$_SESSION['usuario']['nombre']} </h3>";


?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Document</title>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css' rel='stylesheet'
     integrity='sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6' crossorigin='anonymous'>
</head>
<body class="bg-light">
<br>
<h2>Listado de viajes</h2>
<br>
<table class="table">
  <thead class="thead-dark">
                <tr>
                    <th>Nombre</th>
                    <th>Precio</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach (getViajes() as $value) : ?>

                    <tr>
                        <td><?= $value['nombre'] ?></td>
                        <td><?= $value['precio'] ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>

        <form action="" method="post">
        <p><input type='submit' value='Reservar Nuevo viaje' id='reservar' name='reservar'> </p>
        </form>

        <form action="" method="post">

        <p><input type='submit' value='Desconectar: <?=$_SESSION['usuario']['nombre']?>' id='descon' name='descon'> </p>
        </form>
     <?php if (isset($_POST['reservar'])) { 
        
        header('Location:reservar.php');
       
     
      };  ?>
      <?php if (isset($_POST['descon'])) { 
      
        session_unset();
        header('Location:logout.php');

       };  ?>
</body>
<script src='https://code.jquery.com/jquery-3.2.1.slim.min.js'
    integrity='sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN'
    crossorigin='anonymous'></script>
<script src='https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js'
    integrity='sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG' crossorigin='anonymous'></script>
<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js'
     integrity='sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc' crossorigin='anonymous'></script>
</html>