<?php 

define('HOST', 'localhost');
define('USERNAME', 'root');
define('PASSWORD', '');
define('DATABASE', 'agencia');
function getConexionSQLi() {
$conexion = new mysqli(HOST, USERNAME, PASSWORD, DATABASE, 3306);
$conexion->set_charset('utf8');
$error = $conexion->connect_errno;
if ($error != null) {
print'<p>Se ha producido el error:'. $conexion->connect_error.'</p>';
exit();
}
return $conexion;
}

function getConexionPDO() { $opciones = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'); 
 $conexion = new PDO('mysql:host=localhost;dbname='.'agencia','root', '',$opciones ); 
 return $conexion; 
}

function compruebaUser($usuario, $password)
{
    $aux=null;
    $conexion = getConexionPDO();
    $consulta = "SELECT *  FROM clientes WHERE id = '$usuario' and password = MD5('$password')";
    if ($resultado = $conexion->query($consulta)) {
        while ($row = $resultado->fetch()) {
            $aux= array(
                'id' => $row['id'],
                'dni' => $row['dni'],
                'nombre' => $row['nombre'],
                'password' => $row['password']

            );
        }
    }
    unset($conexion);
    return $aux;
}
function getViajes() { 
 $conexion= getConexionSQLi(); 
$consulta='SELECT * FROM viajes order by precio asc'; 
 if ($resultado=$conexion->query($consulta)) {
 while ($row = $resultado->fetch_array()) { 
$aux[]=array(
    'id'=>$row['id'],
    'nombre'=>$row['nombre'],
    'precio'=>$row['precio']
); 
}
 }
 $conexion->close();
 return $aux;
}

function insertViaje($id, $personas, $sesioncliente) { 
$conexion= getConexionPDO();
try {
$conexion->beginTransaction();
$insertar=$conexion->prepare('insert into reservas (id_viaje, num_plazas, id_cliente) values (?,?,?)');
$insertar->bindParam(1, $id);
$insertar->bindParam(2, $personas);
$insertar->bindParam(3, $sesioncliente);
if ($insertar->execute()!=true) {
throw new Exception('error al insertar');
}

$conexion->commit();
return true;
}
 catch (Exception $ex) {
echo $ex->getMessage();
$conexion->rollBack();
return false;
}
}

function getReservas() { 
$conexion= getConexionPDO();
$consulta=$conexion->prepare('SELECT clientes.nombre as nombrecliente, viajes.nombre as nombreviaje, reservas.num_plazas as  numplaza, viajes.precio as prec FROM reservas inner join clientes on clientes.id=reservas.id_cliente inner join viajes on viajes.id=reservas.id_viaje');
if ($consulta->execute()) {
while ($row = $consulta->fetch()) {
$aux[]=array(
'nombrecliente'=>$row['nombrecliente'],
'nombreviaje'=>$row['nombreviaje'],
'numplaza'=>$row['numplaza'],
'prec'=>$row['prec']
);
}
}unset($conexion);
return $aux;
}