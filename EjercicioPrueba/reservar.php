<?php 
require_once( 'funciones.php' ); 
session_start();


 echo "<h3>Hola {$_SESSION['usuario']['nombre']} reserva tu viaje</h3>";
echo "<br>";
?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Reservar</title>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css' rel='stylesheet'
     integrity='sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6' crossorigin='anonymous'>
</head>
<body class="bg-light">

<form action="<?= htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="post">
        <p><label for="viaje">Elige tu viaje</label>
            <select name="viaje" id="viaje">
                <?php foreach (getViajes() as  $value) : ?>
                    <option value="<?= $value['id'] ?>"><?= $value['nombre'] ?> (<?= $value['precio'] ?>)€</option>
                <?php endforeach ?>
            </select>
        </p>
        <p> <label for='personas'>Numero de Personas:  </label> <input type='number' name='personas' id='personas'></p>
        <p><input type='submit' value='Enviar' id='enviar' name='enviar'> </p>
        <hr>
    </form>
    <?php if (isset($_POST['enviar'])) { 
            $sesionuser=$_SESSION['usuario']['id'];
        if (insertViaje($_POST['viaje'],$_POST['personas'],$sesionuser)) {
            echo "Se realizo el viaje correctamente";
        }
     };  ?>
    <hr>
    <a href="viajes.php">Volver al listado de viajes</a><br>
    <a href="reservas_realizadas.php">Ver reservas realizadas</a>
    
 <form action="" method="post">

<p><input type='submit' value='Desconectar: <?=$_SESSION['usuario']['nombre']?>' id='descon' name='descon'> </p>
</form>

<?php if (isset($_POST['descon'])) { 

session_unset();
header('Location:logout.php');
};  ?>

</body>
</body>
<script src='https://code.jquery.com/jquery-3.2.1.slim.min.js'
    integrity='sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN'
    crossorigin='anonymous'></script>
<script src='https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js'
    integrity='sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG' crossorigin='anonymous'></script>
<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js'
     integrity='sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc' crossorigin='anonymous'></script>
</html>