<?php 
require_once( 'funciones.php' ); 
session_start();


 echo "<h3>Hola {$_SESSION['usuario']['nombre']} reserva tu viaje</h3>";
echo "<br>";
?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Reservar</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
</head>
<body class="bg-light">
<table class="table">
  <thead class="thead-dark">
            <tr>
            
                  <th scope="col">Cliente</th>
                   <th scope="col">Viaje</th>
                   <th scope="col">Personas</th>
                   <th scope="col">Precio Total</th>
              </tr>
         </thead> 
          <tbody>
          <?php foreach (getReservas() as  $value) : ?>
              <tr>
                  <td><?=$value['nombrecliente']?></td>
                  <td><?=$value['nombreviaje']?></td>
                  <td><?=$value['numplaza']?></td>
                  <td><?=$value['prec']?></td>
              </tr>
              <?php endforeach ?>
          </tbody> 
          </table>
    <a href="viajes.php">Volver al listado de viajes</a><br>
    <a href="reservas_realizadas.php">Ver reservas realizadas</a>
    
 <form action="" method="post">

<p><input type='submit' value='Desconectar: <?=$_SESSION['usuario']['nombre']?>' id='descon' name='descon'> </p>
</form>

<?php if (isset($_POST['descon'])) { 

session_unset();
header('Location:logout.php');
};  ?>

</body>
</body>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</html>