<?php 
require_once( 'funciones.php' ); 
session_start();

if (isset($_POST['enviar'])) { 

    $usuario=compruebaUser($_POST['id'], $_POST['password']);
    if ($usuario!=null) {
        $_SESSION['usuario']=$usuario;
        header('Location:libros.php');
    }else{
        echo "La contraseña o el usuario son incorrectos";
    }

 }; 
if (isset($_POST['enviar2'])) { 

    $usuario2=recuerda($_POST['id2']);
    $_SESSION['usuario2']=$usuario2;

    if ($usuario2!=null) {

        $_SESSION['usuario2']=$usuario2;
        header('Location:recuperar.php');

    }else{
        echo "Introduce tu DNI";
    }

 }; 

?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Login</title>
    <link href='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css' rel='stylesheet'
     integrity='sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6' crossorigin='anonymous'>
</head>
<body class="bg-light">
    <h1>Login De Viajes</h1>
    <hr>
    <form action="<?=htmlspecialchars($_SERVER['PHP_SELF']) ?>" method="post">
    <p> <label for='id'>Identificador:  </label> <input type='text' name='id' id='id'></p>
    <p> <label for='password'>Password:  </label> <input type='password' name='password' id='password'></p>
    <p><input type='submit' value='Enviar' id='enviar' name='enviar'> </p>
    </form>
    <p><a href="registro.php">Registrate si aun no tienes cuenta</a></p>
  <form action="" method="post">
  <p>Olvidaste tu contraseña?, Escribe solo tu DNI para recuperarlo</p>
  <p> <label for='id2'>Identificador:  </label> <input type='text' name='id2' id='id2'></p>
  <p><input type='submit' value='Pulsa para recuperar tu pass' id='enviar2' name='enviar2'> </p>
</form>
</body>
<script src='https://code.jquery.com/jquery-3.2.1.slim.min.js'
    integrity='sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN'
    crossorigin='anonymous'></script>
<script src='https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js'
    integrity='sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG' crossorigin='anonymous'></script>
<script src='https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js'
     integrity='sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc' crossorigin='anonymous'></script>
</html>