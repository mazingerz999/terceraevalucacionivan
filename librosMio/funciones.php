<?php 

//CONEXIONES A BD
// IMPORTANTE QUE NO SE TE OLVIDE CAMBIAR EL NOMBRE A LA BASE DE DATOS
define('HOST', 'localhost');
define('USERNAME', 'root');
define('PASSWORD', '');
define('DATABASE', 'libros');
function getConexionSQLi()
{
    $conexion = new mysqli(HOST, USERNAME, PASSWORD, DATABASE, 3306);
    $conexion->set_charset('utf8');
    $error = $conexion->connect_errno;
    if ($error != null) {
        print '<p>Se ha producido el error:' . $conexion->connect_error . '</p>';
        exit();
    }
    return $conexion;
}
// IMPORTANTE QUE NO SE TE OLVIDE CAMBIAR EL NOMBRE A LA BASE DE DATOS
function getConexionPDO()
{
    $opciones = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
    $conexion = new PDO('mysql:host=localhost;dbname=' . 'libros', 'root', '', $opciones);
    return $conexion;
}
function compruebaUser($usuario, $password)
{
    $aux = null;
    $conexion = getConexionPDO();
    $consulta = "SELECT *  FROM usuarios WHERE dni = '$usuario' and password = MD5('$password')";
    if ($resultado = $conexion->query($consulta)) {
        while ($row = $resultado->fetch()) {
            $aux = array(
                'id' => $row['id'],
                'nombre' => $row['nombre'],
                'dni' => $row['dni'],
                'password' => $row['password']

            );
        }
    }
    unset($conexion);
    return $aux;
}
function recuerda($usuario)
{
    $aux = null;
    $conexion = getConexionPDO();
    $consulta = "SELECT * FROM usuarios WHERE dni = '$usuario' ";
    if ($resultado = $conexion->query($consulta)) {
        while ($row = $resultado->fetch()) {
            $aux = array(
                'id' => $row['id'],
                'nombre' => $row['nombre'],
                'dni' => $row['dni'],
                'password' => $row['password']

            );
        }
    }
    unset($conexion);
    return $aux;
}

function insertarUsuario($nombre, $dni, $password){
    $conexion = getConexionSQLi();
    $todoOK = true;
    $insertar = $conexion->prepare('insert into usuarios  (nombre,  dni, password) values (?,?,?)');
    /*recuerda que en el bind_param de mysqli hay que meter el tipo de dato*/
    $insertar->bind_param('sss', $nombre,  $dni, $password);
    if ($insertar->execute() != true) {
        $todoOK = false;
    }
    return $todoOK;
}
// function getLibreria(){
//     $aux = null;
//     $conexiom = getConexionSQLi();
//     $consulta = $conexiom->stmt_init();
//     $consulta->prepare("SELECT * FROM viaje order by precio asc");
//     $consulta->execute();
//     $consulta->bind_result($id, $nombre, $precio);
//     while ($consulta->fetch()) {
//         $aux[] = array(
//             "id" => $id, "nombre" => $nombre, "precio" => $precio
//         );
//     }
//     $consulta->close();
//     return $aux;
// }
function getEditorial(){
    $aux = null;
    $conexiom = getConexionSQLi();
    $consulta = $conexiom->stmt_init();
    $consulta->prepare("SELECT * FROM editorial");
    $consulta->execute();
    $consulta->bind_result($id, $nombre, $direccion);
    while ($consulta->fetch()) {
        $aux[] = array(
             "id" => $id,
             "nombre" => $nombre,
             "direccion" => $direccion
        );
    }
    $consulta->close();
    return $aux;
}
function getLibro($ideditorial)
{
    //CREO LA CONEXION
    $conexiom = getConexionSQLi();
    //HAGO  EL STMT INIT
    $consulta = $conexiom->stmt_init();
    //HAGO EL PREPARE  CON LA CONSULTA
    $consulta->prepare("SELECT Numero, Titulo, Anio, Precio, Fecha, nombre, direccion FROM `libro` inner join editorial on id_editorial=id where id_editorial=?");
    //BIND PARAN CON EL TIPO  DE  DATO Y VARIABLE AUX
    $consulta->bind_param("s", $ideditorial);
    //EJECUTO
    $consulta->execute();
    //HAGO LAS VARIABLES DE  LO QUE QUIERO en este caso peso y nombre ordenadas
    $consulta->bind_result($Numero, $Titulo, $Anio, $Precio,$Fecha,$nombre,$direccion,);
    //SACO LOS DATOS CON EL  FETCH
    while ($consulta->fetch()) {
        //GUARDO EN AUX
        $aux[] = array(
            //EL ORDEN ESTE MEJOR PONLO SIEMPRE COMO EN LA QUERY
            "Numero" => $Numero, "Titulo" => $Titulo, "Anio" => $Anio, "Precio" => $Precio, "Fecha" => $Fecha, "nombre" => $nombre,"direccion" => $direccion,
        );
    }
    //CIERRO CONSULTA
    $consulta->close();
    return $aux;
}
function getLibro2($idlibro)
{
    //CREO LA CONEXION
    $conexiom = getConexionSQLi();
    //HAGO  EL STMT INIT
    $consulta = $conexiom->stmt_init();
    //HAGO EL PREPARE  CON LA CONSULTA
    $consulta->prepare("SELECT Numero, Titulo, Anio, Precio, Fecha FROM libro WHERE Numero=?");
    //BIND PARAN CON EL TIPO  DE  DATO Y VARIABLE AUX
    $consulta->bind_param("s", $idlibro);
    //EJECUTO
    $consulta->execute();
    //HAGO LAS VARIABLES DE  LO QUE QUIERO en este caso peso y nombre ordenadas
    $consulta->bind_result($Numero, $Titulo, $Anio, $Precio,$Fecha);
    //SACO LOS DATOS CON EL  FETCH
    while ($consulta->fetch()) {
        //GUARDO EN AUX
        $aux[] = array(
            //EL ORDEN ESTE MEJOR PONLO SIEMPRE COMO EN LA QUERY
            "Numero" => $Numero, "Titulo" => $Titulo, "Anio" => $Anio, "Precio" => $Precio, "Fecha" => $Fecha, 
        );
    }
    //CIERRO CONSULTA
    $consulta->close();
    return $aux;
}