<?php 
require_once( 'funciones.php' );
session_start();
if ($_SESSION['usuario']!=null) {
    echo "Bienvenido {$_SESSION['usuario']['nombre']}"."<br>";
$aux=$_SESSION['libros'];

}else{
    header('Location:index.php');
}
?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Document</title>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'
        integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'>
</head>
<body>
  <br> 
  <form action="" method="post">
  <table  class="table-sm table-hover">
                <thead class="table-info">
                    <tr>          
                        <th>Numero</th>
                        <th>Titulo</th>
                        <th>Año</th>
                        <th>Precio</th>
                        <th>Fecha</th>

                    </tr>
                </thead>
                <tbody>
                <?php foreach ($aux as $key => $value):?>
                    <tr>  
                    <?php foreach ($value as  $prod) : ?>                       
                        <td><?=$prod['Numero']?></td>
                        <td><?=$prod['Titulo']?></td>
                        <td><?=$prod['Anio']?></td>
                        <td><?=$prod['Precio']?></td>
                        <td><?=$prod['Fecha']?></td>
                        <?php endforeach ?>  
                    </tr>  
                  <?php endforeach;?>      
                </tbody>
            </table>
            <p><input type='submit' value='Finalizar' id='Finalizar' name='Finalizar'> </p>
            </form>
            <?php if (isset($_POST['Finalizar'])) { 

             echo "Felicidades por tu compra";
             session_unset();
             header('Location:index.php');
            
             };  ?>
</body>
<script src='https://code.jquery.com/jquery-3.2.1.slim.min.js'
    integrity='sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN'
    crossorigin='anonymous'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js'
    integrity='sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q'
    crossorigin='anonymous'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'
    integrity='sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl'
    crossorigin='anonymous'></script>
</html>