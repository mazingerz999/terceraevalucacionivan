<?php 
require_once( 'funciones.php' );
session_start();
if ($_SESSION['usuario']!=null) {
    echo "Bienvenido {$_SESSION['usuario']['nombre']}";
}else{
    header('Location:index.php');
}

//GUARDO EN LA SESION LIBROS COMPRADOS GUARDDADOS EN UN ARRAY AUXILIAR
if (isset($_POST['Comprar'])) { 
    $idlibros=$_POST['idslibroscheck'];

    for ($i=0; $i <count($_POST['idslibroscheck']) ; $i++) { 
      $libroscesta[]=getLibro2($idlibros[$i]);
    }
    $_SESSION['libros']=$libroscesta;
    header('Location:cesta.php');
 }; 
////////////////////////////////////////

?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Document</title>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'
        integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'>
</head>
<body>
<h1>Libros</h1>
<form action="" method="post">
    <p><label for='editorial'>Seleccione Editorial: <select name="editorial" id="editorial">
    <?php foreach (getEditorial() as  $value) : ?>
    <option value="<?=$value['id']?>"><?=$value['nombre']?></option>
    <?php endforeach ?>
    </select>
    <p><input type='submit' value='Mostrar Libros' id='enviar' name='enviar'> </p>
    </p>
    </form>
    
    <?php if (isset($_POST['enviar'])) : ?>
     <form action="" method="post">
    <h2>Elige los libros que quieras comprar</h2>
            <table  class="table-sm table-hover">
                <thead class="table-info">
                    <tr>          
                        <th>Titulo</th>
                        <th>Año</th>
                        <th>Precio</th>
                        <th>Fecha</th>
                        <th>Editorial</th>
                        <th>Direccion</th>
                    </tr>
                </thead>
                <tbody>
                  <?php foreach (getLibro($_POST['editorial']) as $prod):?>
                    <tr>        
                        <td> <input type="checkbox" name="idslibroscheck[]" value="<?=$prod['Numero']?>"/><?=$prod['Titulo']?></td>
                        <td><?=$prod['Anio']?></td>
                        <td><?=$prod['Precio']?></td>
                        <td><?=$prod['Fecha']?></td>
                        <td><?=$prod['nombre']?></td>
                        <td><?=$prod['direccion']?></td>
                    </tr>  
                  <?php endforeach;?>                      
                </tbody>
            </table>
            <p><input type='submit' value='Comprar' id='Comprar' name='Comprar'> </p>
        </form>
    <?php endif; ?>
</body>
<script src='https://code.jquery.com/jquery-3.2.1.slim.min.js'
    integrity='sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN'
    crossorigin='anonymous'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js'
    integrity='sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q'
    crossorigin='anonymous'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'
    integrity='sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl'
    crossorigin='anonymous'></script>
</html>



