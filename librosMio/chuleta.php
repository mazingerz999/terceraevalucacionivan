<!-- ///////////
/*DB::delete('delete users where name = ?', ['John'])*/
////////// -->
<?php
///Copiar esto al empezar
//POSIBLES ERRORES
// IMPORTANTE QUE NO SE TE OLVIDE CAMBIAR EL NOMBRE A LA BASE DE DATOS
//SI NO ENVIA DATOS, ASEGURATE DE QUE ENVIAS EL FORMULARIO, PORQUE SIEMPRE SE TE PASA

//PARA EL ENVIO DE SESIONES DE USERS A OTRAS PAGINAS ATRAVES DE UN FORMULARIO ES ASI
// <?php 
require_once( 'funciones.php' ); 
session_start();

 if (isset($_POST['enviar'])) { 

    $usuario=compruebaUser($_POST['id'], $_POST['password']);

    if ($usuario!=null) { //ESTO DE DIFERENTE DE NULL TAMBIEN SE APLICA A CUALQUIER SESION QUE QUIERA COMPROBAR QUE TIENE OBJETOS, AL SER NULL NO TIENE NADA.
        $_SESSION['usuario']=$usuario;
        header('Location:viajes.php');
    }else{
        echo "La contraseña o el usuario son incorrectos";
    }

 }; 

//PARA EL ENVIO DE SESIONES DE ARRAYS ES ASI:

if (isset($_POST['enviar'])) { 

        $_SESSION['array']=$objetos;
        // Si el array saca varias tablas se muestra en la siguiente pagina de la seguiente manera
        $_SESSION['nombredelasesion']['nombredelrow,porejemplonombre'];
        header('Location:PAGINA.php');

 }; 

//EN LAS OTRAS PAGINAS PAGINA PHP METER ESTE CODIGO AL EMPEZAR, MODIFICANDO LOS PARAMETROS A LOS DE LA BASE DE DATOS
require_once( 'funciones.php' ); 
session_start();

if ($_SESSION['usuario']==null) {
    header('Location:PAGINA.php');
}else{
    echo "<h3>Bienvenido  {$_SESSION['usuario']['nombre']} reserva tu viaje</h3>";
}

echo "<br>";

//SI QUIERES DESTRUIR LA SESION Y QUE TE MANDE A OTRA WEB MEDIANTE SUBMIT Y FORMULARIO

if (isset($_POST['descon'])) { 

    session_unset();
    header('Location:logout.php');
    };  
//////////////////////////////////////////////
//CONEXIONES A BD
// IMPORTANTE QUE NO SE TE OLVIDE CAMBIAR EL NOMBRE A LA BASE DE DATOS
define('HOST', 'localhost');
define('USERNAME', 'root');
define('PASSWORD', '');
define('DATABASE', 'libros');
function getConexionSQLi()
{
    $conexion = new mysqli(HOST, USERNAME, PASSWORD, DATABASE, 3306);
    $conexion->set_charset('utf8');
    $error = $conexion->connect_errno;
    if ($error != null) {
        print '<p>Se ha producido el error:' . $conexion->connect_error . '</p>';
        exit();
    }
    return $conexion;
}
// IMPORTANTE QUE NO SE TE OLVIDE CAMBIAR EL NOMBRE A LA BASE DE DATOS
function getConexionPDO()
{
    $opciones = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');
    $conexion = new PDO('mysql:host=localhost;dbname=' . 'libros', 'root', '', $opciones);
    return $conexion;
}

//FUNCION COMPRUEBA USER, RECUERDA QUE LA PASS DE LOS USUARIOS TIENEN QUE ESTAR EN MD5 EN LA BASEE DE DATOS///
function compruebaUser($usuario, $password)
{
    $aux = null;
    $conexion = getConexionPDO();
    $consulta = "SELECT *  FROM clientes WHERE id = '$usuario' and password = MD5('$password')";
    if ($resultado = $conexion->query($consulta)) {
        while ($row = $resultado->fetch()) {
            $aux = array(
                'id' => $row['id'],
                'dni' => $row['dni'],
                'nombre' => $row['nombre'],
                'password' => $row['password']

            );
        }
    }
    unset($conexion);
    return $aux;
}
////////////////////////////////////////////////////////////////////////////

//MYSQLI
//PREPARADAS MYSQLI///////////////////////////////////////////////////////////
function getmysqli1($getEquipo)
{
    $aux=null;
    $conexiom = getConexionSQLi();
    $consulta = $conexiom->stmt_init();
    $consulta->prepare("select nombre, peso, nombre_equipo, codigo from jugadores where nombre_equipo=?");
    $consulta->bind_param("s", $getEquipo);
    $consulta->execute();
    $consulta->bind_result($nombre, $peso, $codigo, $nombre_equipo);
    while ($consulta->fetch()) {
        $aux[] = array(
            "nombre" => $nombre, "peso" => $peso, "codigo" => $codigo, "nombre_equipo" => $nombre_equipo,
        );
    }
    $consulta->close();
    return $aux;
}
function getmysqli2()
{
    $aux = null;
    $conexiom = getConexionSQLi();
    $consulta = $conexiom->stmt_init();
    $consulta->prepare("SELECT * FROM viaje order by precio asc");
    $consulta->execute();
    $consulta->bind_result($id, $nombre, $precio);
    while ($consulta->fetch()) {
        $aux[] = array(
            "id" => $id, "nombre" => $nombre, "precio" => $precio
        );
    }
    $consulta->close();
    return $aux;
}
function insertar($titulo, $anyo, $precio, $fecha)
{
    $conexion = getConexionSQLi();
    $todoOK = true;
    $insertar = $conexion->prepare('insert into libro  (Titulo,  Anio, Precio, Fecha) values (?,?,?,?)');
    /*recuerda que en el bind_param de mysqli hay que meter el tipo de dato*/
    $insertar->bind_param('siis', $titulo,  $anyo, $precio, $fecha);
    if ($insertar->execute() != true) {
        $todoOK = false;
    }
    return $todoOK;
}
function delete($titulo)
{
    $conexion = getConexionSQLi();
    $todoOK = true;
    $borrar = $conexion->prepare('DELETE FROM libro WHERE Numero=?');
    $borrar->bind_param('s', $titulo);
    if ($borrar->execute() != true) {
        $todoOK = false;
    }
    return $todoOK;
}

//TRANSACCIONES MYSQLI
function Transaccion1()
{
    $conexion = getConexionSQLi();
    $todoOk = true;
    $conexion->autocommit(false);
    $borrado = $conexion->prepare('DELETE  FROM pasajeros');
    if ($borrado->execute() != true) {
        $todoOk = false;
    }
    $update = $conexion->prepare("UPDATE plazas SET reservada = '0'");

    if ($update->execute() != true) {
        $todoOk = false;
    }
    if ($todoOk) {
        $conexion->commit();
        return true;
    } else {
        $conexion->rollBack();
        return false;
    }
}
function Transaccion2($jugadorBaja, $nombre, $procedencia, $altura, $peso, $posicion, $equipo)
{
    $conexion = getConexionSQLi();
    $todoOk = true;
    $conexion->autocommit(false);
    $borrado = $conexion->prepare('DELETE  FROM jugadores WHERE nombre=?');
    $borrado->bind_param('s', $jugadorBaja);
    if ($borrado->execute() != true) {
        $todoOk = false;
       
    }
    $update = $conexion->prepare('INSERT INTO jugadores (codigo, nombre, procedencia, altura, peso, posicion, nombre_equipo)
     VALUES ((SELECT (t.codigo + 1) from jugadores AS t ORDER BY t.codigo DESC LIMIT 1),?,?,?,?,?,?)');
    $update->bind_param('ssddss', $nombre, $procedencia, $altura, $peso, $posicion, $equipo);
    if ($update->execute() != true) {
        $todoOk = false;
    }
    if ($todoOk) {
        $conexion->commit();
        return true;
    } else {
        $conexion->rollBack();
        return false;
    }
}
//MYSQLI
//FIN MYSQLI///////////////////////////////////////////////////////////

//PDO
//PREPATADAS PDO
function getJugadores($equipo)
{
    $aux = null;
    $conexion = getConexionPDO();
    $consulta = $conexion->prepare('select * from jugadores where nombre_equipo=?');
    $consulta->bindParam(1, $equipo);
    if ($consulta->execute()) {
        while ($row = $consulta->fetch()) {
            $aux[] = array(
                'nombre' => $row['nombre'],
                'peso' => $row['peso'],
                'nombre_equipo' => $row['nombre_equipo'],
                'codigo' => $row['codigo']
            );
        }
    }
    unset($conexion);
    return $aux;
}
function insertpdo($titulo, $anyo, $precio, $fecha)
{
    $aux = null;
    $conexion = getConexionPDO();
    $todoOK = true;
    $insertar = $conexion->prepare('insert into libro (titulo,  anio, precio, fecha) values (?,?,?,?)');
    $insertar->bindparam(1, $titulo);
    $insertar->bindparam(2, $anyo);
    $insertar->bindparam(3, $precio);
    $insertar->bindparam(4, $fecha);
    if ($insertar->execute() != true) {
        $todoOK = false;
    }
    return $todoOK;
}
function deletepdo($titulo) {
    $conexion= getConexionPDO();
    $todoOK=true;
    $borrar=$conexion->prepare('DELETE FROM libro WHERE titulo=?');
    $borrar->bindParam(1, $titulo);
    if ($borrar->execute()!=true) {
    $todoOK=false;
    }
    return $todoOK;
    }
function updatepdo($peso, $nombre)
{
    $conexion = getConexionPDO();
    $todoOk = true;
    $update = $conexion->prepare('UPDATE jugadores SET peso = ? WHERE nombre = ?');
    $update->bindParam(1, $peso);
    $update->bindParam(2, $nombre);
    if ($update->execute() != true) {
        $todoOk = false;
    }
    return $todoOk;
}

//TRANSACCIONES PDO
function transacccionpdo1($jugadorBaja, $nombre, $procedencia, $altura, $peso, $posicion, $equipo)
{
    $conexion = getConexionPDO();
    try {
        $conexion->beginTransaction();
        $borrar = $conexion->prepare('delete from jugadores where nombre=?');
        $borrar->bindParam(1, $jugadorBaja);
        if ($borrar->execute() != true) {
            throw new Exception('error al borrar');
        }
        $insertar = $conexion->prepare('INSERT INTO jugadores (codigo, nombre, procedencia, altura, peso, posicion, nombre_equipo)
         VALUES ((SELECT (t.codigo + 1) from jugadores AS t ORDER BY t.codigo DESC LIMIT 1),?,?,?,?,?,?)');

        $insertar->bindParam(1, $nombre);
        $insertar->bindParam(2, $procedencia);
        $insertar->bindParam(3, $altura);
        $insertar->bindParam(4, $peso);
        $insertar->bindParam(5, $posicion);
        $insertar->bindParam(6, $equipo);
        if ($insertar->execute() != true) {
            throw new Exception('error al insertar');
        }
        $conexion->commit();
        return true;
    } catch (Exception $ex) {
        echo $ex->getMessage();
        $conexion->rollBack();
        return false;
    }
}

function TransaccionReservara($nombre, $dni, $asiento)
{
    $conexion = getConexionPDO();
    try {
        $conexion->beginTransaction();
        $borrar = $conexion->prepare("update plazas set reservada=1 where numero=?");
        $borrar->bindParam(1, $asiento);
        if ($borrar->execute() != true) {
            throw new Exception('error al borrar');
        }
        //PARA DAR UN VALOR VACIO ES LA m DEL BINDPARAM
        $insertar = $conexion->prepare("insert into pasajeros value (?,?,'m',?)");
        $insertar->bindParam(1, $nombre);
        $insertar->bindParam(2, $dni);
        $insertar->bindParam(3, $asiento);

        if ($insertar->execute() != true) {
            throw new Exception('error al insertar');
        }
        $conexion->commit();
        return true;
    } catch (Exception $ex) {
        echo $ex->getMessage();
        $conexion->rollBack();
        return false;
    }
}
//FIN TRANSACCIONES PDO
////////////////////////////////////////////////////////////////////////
//FIN PDO