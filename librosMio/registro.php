<?php 
require_once( 'funciones.php' );
session_start();

    if (isset($_POST['enviar'])) { 
        if (insertarUsuario($_POST['dni'],$_POST['nombre'],$_POST['password'])==true) {
            echo "Ya estas registrado";
            echo "<br>";
            echo "<a href='index.php'>Login</a>";
        }
    };
?>

<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
    <meta http-equiv='X-UA-Compatible' content='ie=edge'>
    <title>Index</title>
    <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css'
        integrity='sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm' crossorigin='anonymous'>
</head>
<body>
    <h1>Registrate</h1>
    <br>
   
    <form action="" method="post">
    <p> <label for='dni'>Introduce tu dni:  </label> <input type='text' name='dni' id='dni'></p>
    <p> <label for='nombre'>Introduce tu nombre:  </label> <input type='text' name='nombre' id='nombre'></p>
    <p> <label for='password'>Introduce tu password:  </label> <input type='text' name='password' id='password'></p>
    <p><input type='submit' value='Enviar' id='enviar' name='enviar'> </p>
    </form>

</body>
<script src='https://code.jquery.com/jquery-3.2.1.slim.min.js'
    integrity='sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN'
    crossorigin='anonymous'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js'
    integrity='sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q'
    crossorigin='anonymous'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js'
    integrity='sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl'
    crossorigin='anonymous'></script>
</html>